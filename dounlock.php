#!/usr/bin/php -q
<?php

/**
 * Para realizar o hangup insira o conteudo abaixo dentro do extensions.conf
 * 
 * exten => _55XXXX,1,NoOp(Validando Hangup)
 * exten => _55XXXX,n,Authenticate(1234)
 * exten => _55XXXX,n,AGI(dounlock.php,${EXTEN:2})
 * exten => _55XXXX,n,NoOp(${CANAL})
 */

// Imports
require_once('bmconnector/api/AsteriskManagerAPI.php');
require_once('bmconnector/config/Bootstrap.php');
require_once ('phpagi/phpagi.php');

$agi = new AGI();
$asterisk = new AsteriskManagerAPI();

$ramal = $argv[1];
$ps = $asterisk->getChannel($ramal);

$agi->hangup($ps);
$agi->set_variable("CANAL",$ps);

exit ();
?>