#!/usr/bin/php
<?php

require_once('phpagi/phpagi.php');

$agi=new AGI();

$ramal=$argv[1];

/**
 * Operadoras validas para portabilidade
 * inserir novas de acordo com a necessidade
 */
$ramaisValidos = array (
        '6010', '6011', '6012', '6013', '6014',
        '6015', '6016', '6017', '6018', '6019',
        '6020', 
);

$response = in_array($ramal, $ramaisValidos) ? 'true' : 'false';
$agi->set_variable("CONTAINS", $response);
exit();

?>

