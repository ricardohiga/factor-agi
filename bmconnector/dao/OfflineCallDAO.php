#!/usr/bin/php
<?php

/**
 * Classe DAO para o modelo OnlineCall
 * @author Fabricio S Costa fabriciojf@gmail.com
 * @since 28/05/2014
 * @version 1.0
 */
class OfflineCallDAO {

/**
 * Seta uma ligacao como atendida no banco de dados
 * para chamadas via Queues
 */
	public function answerSainte($origin) {
		$pers = new Persistence();

		$sql = "UPDATE offline_calls SET ";
		$sql .= " in_queue=false,";
		$sql .= " status=3,";
		$sql .= " dialstatus='ANSWER',";
		$sql .= " dh_start='" .date('Y-m-d H:i:s', strtotime("now")). "'";
		$sql .= " WHERE origin='" .$origin. "'";
		$sql .= " AND in_queue='true'";
		$pers->execute($sql);
	}
	
/**
 * Atende uma ligacao no banco de dados 
 * para chamadas via Dial
 */
	public function answerEntrante($destination='',$origin='',$dstchannel='') {
		$pers = new Persistence();
		
		$sql = "UPDATE offline_calls SET ";
		$sql .= " in_queue=true,";
		$sql .= " destination='" .$destination. "',";
		$sql .= " dstchannel='" .$dstchannel. "',";
		$sql .= " status=3,";
		$sql .= " dialstatus='ANSWER',";
		$sql .= " dh_start='" .date('Y-m-d H:i:s', strtotime("now")). "'";
		$sql .= " WHERE origin='" .$origin. "'";
		$sql .= " AND in_queue='true'";
		$pers->execute($sql);
	}	

/**
 * Atende uma ligacao no banco de dados
 * para chamadas via Dial
 */
	public function answerEntranteDial($origin='') {
		$pers = new Persistence();
	
		$sql = "UPDATE offline_calls SET ";
		$sql .= " in_queue=true,";
		$sql .= " status=3,";
		$sql .= " dialstatus='ANSWER',";
		$sql .= " dh_start='" .date('Y-m-d H:i:s', strtotime("now")). "'";
		$sql .= " WHERE origin='" .$origin. "'";
		$sql .= " AND in_queue='true'";
		$pers->execute($sql);
	}
	
}
