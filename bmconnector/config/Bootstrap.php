<?php

/**
 * Classe contendo informacoes sobre o Server e Asterisk
 * 
 * @author Fabricio S Costa fabriciojf@gmail.com
 * @since ${date}
 * @version 1.0
 */
class Bootstrap {

        public static $data = array(
            'System.host' => '172.16.44.248',
            'System.hostscob' => '172.16.44.249',
            'System.name' => 'bmtelecom',
        );
        
        /**
         * Retorna o parametro $pamareter
         * @param unknown $parameter
         */
        public static function read($parameter) {
                return self::$data[$parameter];
        }
}
