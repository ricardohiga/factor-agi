#!/usr/bin/php
<?php

/**
 * Controller de entrada do sistema
 */
class Click2CallsController {
	
/**
 * executa o metodo sainte no aplicativo remoto
 */	
	public function sainte($callid='',$uniqueid='') {
		$confs = new Bootstrap();
		$url = sprintf("http://%s/%s/connector/sainte_ctc/%s/%s",
				$confs->read('System.host'),
				$confs->read('System.name'),
				$callid,
				$uniqueid
		);	
		return $url;
	}
	
/**
 * executa o metodo answer_sainte no aplicativo remoto
 */
	public function answer_sainte($uniqueid='') {
		$confs = new Bootstrap();
		$url = sprintf("http://%s/%s/connector/answer_sainte_ctc/%s",
				$confs->read('System.host'),
				$confs->read('System.name'),
				$this->clean($uniqueid)
		);
		return $url;
	}	
	
/**
 * executa o metodo close_sainte no aplicativo remoto
 */
	public function close_sainte($uniqueid='') {	
		$confs = new Bootstrap();
		$url = sprintf("http://%s/%s/connector/close_sainte_ctc/%s",
				$confs->read('System.host'),
				$confs->read('System.name'),
				$this->clean($uniqueid)
		);
		return $url;
	}
	
/**
 * limpa a string passada em value 
 */
	public function clean($value) {
		return str_replace("/", "=D5", $value);
	}
}
?>
