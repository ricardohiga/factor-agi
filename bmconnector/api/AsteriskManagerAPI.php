<?php

/**
 * Classe para manipulacao do Asterisk, com login via ManagerAPI
 * @author Lenita Ambrosio lenita@bemmelhor.com.br
 *         Fabricio S Costa fabriciojf@gmail.com
 * @since 08/08/2014
 * @version 1.0
 */
class AsteriskManagerAPI {
    
    var $socket;
    var $error;

/**
 * Construtor
 */
    public function AsteriskManagerAPI()
    {
        $this->socket = FALSE;
        $this->error = "";
    }
    
/**
 * login()
 * 
 * Faz login no Asterisk utilizando as configuracoes do config/Bootstrap.php
 * @return boolean
 */
    public function login() {

        $host = Bootstrap::read("Asterisk.host");
        $username = Bootstrap::read("Asterisk.username");
        $password = Bootstrap::read("Asterisk.password");
        
        $this->socket = @fsockopen($host,"5038", $errno, $errstr, 1);
        if (!$this->socket) {
            $this->error =  "Could not connect - $errstr ($errno)";
            return FALSE;
        }else{
            stream_set_timeout($this->socket, 1);

            $wrets = $this->Query("Action: Login\r\nUserName: $username\r\nSecret: $password\r\nEvents: off\r\n\r\n");
            if (strpos($wrets, "Message: Authentication accepted") != FALSE){
                return true;
            }else{
                $this->error = "Could not login - Authentication failed";
                fclose($this->socket);
                $this->socket = FALSE;
                return FALSE;
            }
        }
    }

/**
 * logout()
 * 
 * Faz logout no Asterisk
 */
    public function logout(){
    	$wrets = "";
        if ($this->socket){
            fputs($this->socket, "Action: Logoff\r\n\r\n");
            while (!feof($this->socket)) {
                $wrets .= fread($this->socket, 8192);
            }
            fclose($this->socket);
            $this->socket = "FALSE";
        }
        return;
    }

/**
 * query()
 * 
 * Executa uma query dentro do Asterisk e retorna a saida do console
 * @param unknown_type $query
 */    
    public function query($query){
        $wrets = "";

        if ($this->socket === FALSE)
            return FALSE;

        fputs($this->socket, $query);
        do
        {
            $line = fgets($this->socket, 4096);
            $wrets .= $line;
            $info = stream_get_meta_data($this->socket);
        }while ($line != "\r\n" && $info['timed_out'] == false );
        return $wrets;
    }

/**
 * getError()
 * 
 * Retorna o erro contido na instÃĒncia de classe
 */
    public function getError(){
        return $this->error;
    }
    
/**
 * getPeers()
 * 
 * Lista todos os peers registrados no Asterisk
 */    
    public function getPeers() {
        
        $this->login();
        
        $value = $this->query("Action: Command\r\nCommand: sip show peers\r\n\r\n");        
        $this_array = preg_split("/\n/",$value);
        
        $peers = array();
        
        foreach($this_array as $line)
        {
            if ((!strstr($line, "Response")) && 
                   (!strstr($line, "Privilege")) &&
                   (!strstr($line, "sip peers")) &&
                   (!strstr($line, "COMMAND")) &&
                   (!strstr($line, "Name"))) {
                
                array_push($peers, $line);
            }
        }

        $this->logout();
        return $peers;
    }
    
/**
 * getChannels()
 * 
 * Lista todos os channels ativos no Asterisk
 */    
    public function getChannels() {
            
        $this->login();
    
        $value = $this->query("Action: Command\r\nCommand: core show channels\r\n\r\n");
        $this_array = preg_split("/\n/",$value);
    
        $channels = array();
    
        foreach($this_array as $line)
        {
            if ((!strstr($line, "Channel")) &&
                    (!strstr($line, "Location"))) {    
                array_push($channels, $line);
            }
        }
    
        $this->logout();
        return $channels;
    }   
    
 /**
 * getKhompChannels()
 * 
 * Lista os canais da placa Khomp
 */    
    public function getKhompChannels() {
        
        $this->login();
        
        $value = $this->query("Action: Command\r\nCommand: khomp channels show\r\n\r\n");        
        $this_array = preg_split("/\n/",$value);
        
        $channels = array();

        foreach($this_array as $line)
        {
            if ((strstr($line, "|")) &&
            	   (!strstr($line, "Connections")) && 
                   (!strstr($line, "hw")) &&
                   (!strstr($line, "type")) &&
                   (!strstr($line, "----------"))) {
                array_push($channels, $line);
            }
        }

        $this->logout();
        return $channels;
    } 
    
/**
 * requestHangup()
 *
 * Lista todos os channels ativos no Asterisk
 */
    public function requestHangup($channel) {
        try {    
            $this->login();    
            $value = $this->query("Action: Command\r\nCommand: hangup request $channel\r\n\r\n");
            $this->logout();
        } catch(Exception $e) {
            return "Fail";
        }        
        return "Sucess";
    }
}