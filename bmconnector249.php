#!/usr/bin/php
<?php

/**
 * Classe de entrada do BmConnector
 * Deve tratar as chamadas saintes e os hangups
 * para o bom funcionamento do sistema preditivo
 * 
 * @author Fabricio S Costa
 * @version 3.2.2
 * @since 2016/01/05
 */

/**
 * Imports
 */
require_once ('bmconnector/config/Bootstrap.php');
require_once ('bmconnector/tools/StringTools.php');
require_once ('phpagi/phpagi.php');

/**
 * Instanciando os objetos de classe
 */
$agi = new AGI();
$confs = new Bootstrap();

$url = '';

/**
 * Data utc
 */
$timezone = new DateTimeZone('UTC');
$date = new DateTime("now", $timezone);
$utc = $date->format('Y-m-d H:i:s');

switch (strtolower($argv[1])) {

	/**
	 * sainte pode conter a informacao do preditivo
	 * Variaveis enviadas pelo preditivo 
	 * exten => s,n,AGI(bmconnector.php,Sainte,${UNIQUEID},${ORIGEM},${DESTINO},${DISCAGEM},${CDR(linkedid)},${PREDICTIVE},${QUEUE},${QUEUEID},${ITEMID})
	 * 
	 * $argv[1] = 'sainte'
	 * $argv[2] = ${UNIQUEID}
	 * $argv[3] = ${ORIGEM}
	 * $argv[4] = ${DESTINO}
	 * $argv[5] = ${DISCAGEM}
	 * $argv[6] = ${CDR(linkedid)}
	 * $argv[7] = ${PREDICTIVE}
	 * $argv[8] = ${QUEUE}
	 * $argv[9] = ${QUEUEID}
	 * $argv[10] = ${ITEMID}
	 */
	case 'sainte':
	
	    if (isset ($argv[7]) && !empty($argv[7])) {	    	
			$url = sprintf("http://%s/%s/connector/predictive/%s/%s/%s/%s/%s/%s/%s",
					$confs->read('System.hostscob'),
					$confs->read('System.name'),
					$argv[2],
					$argv[3],
					$argv[4],
					$argv[6],
					$argv[9],
					$argv[8],
					$argv[10]
				);
			
		}		
		break;

	/**
	 * exten => s,n,AGI(bmconnector.php,AnswerSainte,${ARG1})
	 * 
	 * $argv[1] = 'answersainte'
	 * $argv[2] = ${CDR(uniqueid)}
	 * $argv[3] = ${CDR(linkedid)}
	 */
	case 'answersainte':
		$url = sprintf("http://%s/%s/connector/answer_sainte/%s/%s",
				$confs->read('System.hostscob'),
				$confs->read('System.name'),
				$argv[2],
				$argv[3]
			);
		break;
		
	/**
	 exten => s,n,AGI(bmconnector.php,SetAgent,${MEMBERINTERFACE},${CDR(linkedid)})
	 *
	 * $argv[1] = 'setagent'
	 * $argv[2] = ${MEMBERINTERFACE}
	 * $argv[3] = ${CDR(linkedid)}
	 */
	case 'setagent':
		$url = sprintf("http://%s/%s/connector/set_agent/%s/%s",
		$confs->read('System.hostscob'),
		$confs->read('System.name'),
		StringTools::clean($argv[2]),
		$argv[3]
		);
		break;

	/**
	 * $argv[1] = 'closedainte'
	 * $argv[2] = ${UNIQUEID}
	 * $argv[3] = ${LINKEDID}
	 * $argv[4] = ${DIALSTATUS}
	 * $argv[5] = ${HANGUPCAUSE} // 16 = normal Clearing
	 */	
	case 'closesainte':
			
		if ((strtolower($agi->request['agi_callerid']) == 'agentlogin') ||
				($agi->request['agi_dnid'] == '#'.$agi->request['agi_calleridname'])) {	
			$url = sprintf("http://%s/%s/connector/agent_logoff/%s",
					$confs->read('System.hostscob'),
					$confs->read('System.name'),
					$agi->request['agi_calleridname']
				);
		}
		else {
			$url = sprintf("http://%s/%s/connector/close_sainte/%s",
					$confs->read('System.hostscob'),
					$confs->read('System.name'),
					$argv[2]
				);
		}
		
		break;
		
	/**
	 * $argv[1] = 'agentlogin'
	 * $argv[2] = AGENT
	 */	
	case 'agentlogin':
		$url = sprintf("http://%s/%s/connector/agent_login/%s",
				$confs->read('System.hostscob'),
				$confs->read('System.name'),
				$argv[2]
			);
		break;
		
	/** 
	 * $argv[1] = ORIGIN
	 * $argv[2] = DESTINATION
	 */	
	case 'entrante0800':
		$url = sprintf('http://%s/%s/entrante0800s/add/%s/%s',
				$confs->read('System.hostscob'),
				$confs->read('System.name'),
				$argv[1], 
				$argv[2]
			);
		
		break;

	/**
	 * Origin
	 * ramal
	 * uniqueid
	 * linkedid
	 * queue
 	 */
	case 'incoming':
                $url = sprintf('http://%s/%s/connector/incoming/%s/%s/%s/%s/%s',
                                $confs->read('System.hostscob'),
                                $confs->read('System.name'),
                                StringTools::clean($argv[2]), 
                                StringTools::clean($argv[3]),
                                StringTools::clean($argv[4]),
                                StringTools::clean($argv[5]),
                                StringTools::clean($argv[6])
                        );
                
                break;

	default :
		$agi->noop('Metodo nao encontrado');
		break;		
}

$agi->noop('====> URL: ' .$url);

/**
 * Resposta do system via CUrl
 */
$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

exit ();
?>
