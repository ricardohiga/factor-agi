#!/usr/bin/php
<?php

require_once('phpagi/phpagi.php');

$agi=new AGI();

$chave='INSIRAACHAVE';
$cliente='INSIRAONOME';

$numero=$argv[1];
$destination = $agi->request['agi_extension'];

/**
 * Operadoras validas para portabilidade
 * inserir novas de acordo com a necessidade
 */
$opValidas = array (
                '20', // vivo
                '21', // claro
                '23', // vivo
                '31', // oi
                '41', // tim
        );

$agi->verbose("Jumbo Portabilidade: ".$numero);

$urlbm = "http://www.bemmelhor.com.br/";
$urlvpn = "http://179.184.242.189/";

$url = $urlbm. "apps/portabilidade/consultarnumero/$numero/$chave/$cliente";

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

$agi->verbose("Resposta do servidor para o numero " .$numero. ": " .$output);
$operadora = substr($output,-2,2);
$operadora = in_array($operadora, $opValidas) ? $operadora : '00';
$agi->verbose("Operadora: " . $operadora);

$agi->set_variable("OPERADORA",$operadora);
exit();

?>

