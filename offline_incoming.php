#!/usr/bin/php
<?php

require_once('phpagi/phpagi.php');

$agi=new AGI();

/**
 * Recebendo mensagem SMS pela placa
 */

$host = 'www.bemmelhor.com.br/agendamentos'; 

$linkedid = $argv[1];
$url = sprintf("http://%s/rest_api/del_online_incoming/%s",
        $host,$linkedid);

$ch = curl_init($url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
$output = trim(curl_exec($ch));
curl_close($ch);

$agi->noop('=> URL: ' .$url);